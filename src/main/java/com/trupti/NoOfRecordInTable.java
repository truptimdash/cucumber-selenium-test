package com.trupti;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class NoOfRecordInTable {
	String select = "select count(1) from student s ";
	Statement statement;
	Connection conn;
	ResultSet resultSet;
	int size = 0;
	public int getNoOfRecords() {
		
		try {
			JavaToPostgresConnection connection = new JavaToPostgresConnection();
			conn = connection.conect();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(select);
			
			while (resultSet.next()) {
				 size = resultSet.getInt("count");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return size;
	}

	public static void main(String[] args) {
		NoOfRecordInTable selectRecord = new NoOfRecordInTable();
		int num = selectRecord.getNoOfRecords();
		System.out.println("Number of record is:"+num);
	}
}
