package com.trupti;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Test {
	public boolean isEvenNumber(int num) {
		return num % 2 == 0;
	}

	// Create a method will test a number as prime or not
	public boolean isNumberIsPrime(int number) {
		int count = 0;
		for (int i = 2; i < number; i++) {
			if (number % i == 0) {
				count++;
			}
			if (count >= 1) {
				return false;
			}
		}
		return true;
	}

	// Create a method which will return the size of the input String
	public int getSizeOfTheString(String name) {
		return name.length();
	}

	// Create a method to compare two string
	public boolean compareTwoString(String name, String name1) {
		return name.equals(name1);
	}

	// Create a method which will return Upper case of the String
	public String toUpperCase(String name) {
		return name.toUpperCase();
	}

	public String toLowerCase(String name) {
		return name.toLowerCase();
	}

// Create a method which will take list for String as input and sort them in alphabetical order asc
	public List<String> getAsecndingOrder(String number, String number1, String number2) {
		ArrayList<String> list = new ArrayList<String>();
		list.add(number);
		list.add(number1);
		list.add(number2);
		Collections.sort(list);
		return list;
	}

	// Create a method which will take list for String as input and sort them in
	// alphabetical order desc
	public List<String> sortDescending(String number, String number1, String number2) {
		ArrayList<String> list = new ArrayList<String>();
		list.add(number);
		list.add(number1);
		list.add(number2);
		Collections.sort(list, Collections.reverseOrder());
		return list;
	}

	// Create a Date Object for current date
	public String getCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = new Date();
		// Date to String convert.
		// String strDate = formatter.format(date1);
		return formatter.format(date1);
	}

	// Create method to compare to Dates
	public boolean compare(String date, String date3) throws java.text.ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-dd-MM");
		// Convert a String to a Date
		Date date1 = formatter.parse(date);
		Date date2 = formatter.parse(date3);
		if (date1.after(date2)) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) throws java.text.ParseException {
		Test test = new Test();

		System.out.println(test.isNumberIsPrime(4));
		/*
		 * boolean b = test.isEvenNumber(2); System.out.println("The number is Even:" +
		 * b); System.out.println("the number is prime:" + prime); int size =
		 * test.getSizeOfTheString("Sunu"); System.out.println("Size of the name is:" +
		 * size); Boolean comapre = test.compareTwoString("Gelhu", "Gundi");
		 * System.out.println("the compare name is:"+comapre); String upperCase =
		 * test.toUpperCase("Debashish");
		 * System.out.println("UpperCase Value is:"+upperCase); String lowerCase =
		 * test.toLowerCase("Debashish");
		 * System.out.println("lowerCase Value is:"+lowerCase);
		 */
		/*
		 * List<String> sortAsc = test.getAsecndingOrder("one", "two", "four");
		 * System.out.println("Sorted ArrayList in Ascending Order" + sortAsc);
		 * 
		 * List<String> sortDsc = test.sortDescending("one", "two", "four");
		 * System.out.println("Sorted ArrayList in Descending Order" + sortDsc); String
		 * date = test.getCurrentDate(); System.out.println("The current date is:" +
		 * date); // test.compareDate(); boolean comp = test.compare("22-02-2011",
		 * "07-04-2010"); System.out.println("Date2 is before Date1:" + comp);
		 */
	}
}