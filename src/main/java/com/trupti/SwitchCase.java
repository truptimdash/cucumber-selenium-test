package com.trupti;

import java.util.ArrayList;
import java.util.List;

public class SwitchCase {
	public String getTestSwitch(String name) {
		String titel = null;
		switch (name) {
		case "Trupti":
			titel = "Dash";
			break;
		case "Debashish":
			titel = "Das";
			break;
		case "Devesh":
			titel = "Pani";
			break;
		default:
		}
		return titel;
	}

	public void testForLoop() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= 6; j++) {
				System.out.println(i + " " + j);
			}

		}
	}

	public void testCommon() {
		List<String> listOne = new ArrayList<>();
		listOne.add("Debashish");
		listOne.add("Trupti");
		listOne.add("Devesh");
		listOne.add("Tiku");

		List<String> listTiku = new ArrayList<>();
		listTiku.add("Debashish");
		listTiku.add("Trupti");
		listTiku.add("Devesh");
		List<String> commonString = new ArrayList<>();
		for (String string : listOne) {
			for (String string2 : listTiku) {
				if (string.equals(string2)) {
					commonString.add(string);
				}

			}
		}
	}

	public void duplicateList() {
		List<String> listOne = new ArrayList<>();
		listOne.add("Debashish");
		listOne.add("Debashish");
		listOne.add("Trupti");
		listOne.add("Devesh");
		listOne.add("Debashish");
		listOne.add("Devesh");
		listOne.add("Debashish");
		listOne.add("Tiku");
		List<String> duplicateList = new ArrayList<String>();
		for (int i = 0; i < listOne.size(); i++) {
			for (int j = i + 1; j < listOne.size(); j++) {
				if (listOne.get(i).equals(listOne.get(j))) {
					if (!duplicateList.contains(listOne.get(i))) {
						duplicateList.add(listOne.get(i));
						break;
					}

				}
			}
		}
		System.out.println(duplicateList);
	}

	public static void main(String[] args) {
SwitchCase switchCase = new SwitchCase();
switchCase.duplicateList();
	}
}
