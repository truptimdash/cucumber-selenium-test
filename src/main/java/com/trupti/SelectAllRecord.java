package com.trupti;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SelectAllRecord {
	String select = "Select * from student";
	PreparedStatement statement;
	Connection conn;
	ResultSet resultSet;

	public ArrayList<Student> getAllStudents() {
		
		ArrayList<Student> arrayList = new ArrayList<Student>();

		try {
			JavaToPostgresConnection connection = new JavaToPostgresConnection();
			conn = connection.conect();
			statement = conn.prepareStatement(select);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Student student = new Student();
				student.setStuName(resultSet.getString("stu_name"));
				student.setRollNo(resultSet.getString("roll_no"));
				arrayList.add(student);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return arrayList;
	}

	public static void main(String[] args) {
		SelectAllRecord selectRecord = new SelectAllRecord();
		List<Student> list = selectRecord.getAllStudents();
		for (Student student : list) {
			System.out.println(student.getRollNo());
			System.out.println(student.getStuName());
		}
	}
}
