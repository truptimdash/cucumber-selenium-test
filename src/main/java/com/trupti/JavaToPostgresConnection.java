package com.trupti;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JavaToPostgresConnection {
	private final String url = "jdbc:postgresql://localhost:5432/postgres";
	private final String user = "postgres";
	private final String password = "admin";

	public Connection conect() throws SQLException {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(url, user, password);
			if (connection != null) {
				System.out.println("Connected to the PostgreSQL server successfully.");
			} else {
				System.out.println("Connection fail");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return connection;
	}

}
