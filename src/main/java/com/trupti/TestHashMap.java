package com.trupti;

import java.sql.SQLException;
import java.util.HashMap;

public class TestHashMap {
	
	public void testHashMap()
	{
		HashMap<Integer,String> map = new HashMap<Integer,String>();
		map.put(1, "trupti");
		map.put(2, "Sunu");
		map.put(3, "Debashish");
		System.err.println(map);
		System.out.println("Size of the map is:" +map.size());
		System.out.println("Check key is present:" +map.containsKey(1));
		System.out.println("value of the object is:" +map.containsValue("trupti"));
		System.out.println("value of the key 3 is:"+map.get(3));
		for (Integer keyInteger : map.keySet())
		{
			System.out.println("key is:" +keyInteger);
			System.out.println("value is:" +map.get(keyInteger));
		}
		
	}
	
	public static void main(String args[]) throws SQLException {
		TestHashMap hashMap = new TestHashMap();
		hashMap.testHashMap();

	}

}
