package com.trupti;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertValueInTable {
	String insertValue = "insert into student values(?,?) ";
	PreparedStatement statement;
	Connection conn;

	public void insertValue(String name, int rollNo) {

		// Student student = new Student();
		try {
			JavaToPostgresConnection connection = new JavaToPostgresConnection();
			conn = connection.conect();
			statement = conn.prepareStatement(insertValue);
			statement.setString(1, name);
			statement.setInt(2, rollNo);
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		InsertValueInTable selectRecord = new InsertValueInTable();
		selectRecord.insertValue("Devesh", 13);

		System.out.println("he;llo");
		System.out.println("he;llo");
	}
}
