package com.trupti;

import javax.swing.text.Position;

import org.checkerframework.checker.index.qual.SubstringIndexBottom;
import org.checkerframework.checker.units.qual.Length;

public class AllStringMethod {

	public void testString(String s) {
		String s1 = "WellCome" + s.concat("Dash");
		System.out.println("s1 is:" + s1);
		int s2 = s1.compareTo(s);
		System.out.println("s2 is:" + s2);
	}

	public String isSubStringMethod(String string, int index) {
		String newString = string.substring(index);
		return newString;
	}

	public int isChar(String name) {
		name.charAt(5);
		// name.substring(0, 3)
		return name.indexOf("up");
	}

	public char isCharAt(String name) {
		int strLength = name.length();
		Character strChar = name.charAt(strLength - 1);
		return strChar;
	}

	public String isConcat(String name, String titel) {
		String fullName = name.concat(titel);
		return fullName;
	}

	public boolean isEqualMethod(String country, String country1) {
		boolean check = false;
		if (country.equals(country1)) {
			check = true;
		} else {
			check = false;
		}
		return check;
	}

	public String getIndexOf(String name, char ch) {
		int index = name.indexOf(ch);
		String string = name.substring(name.lastIndexOf(ch));
		return string;
	}

	public void isSplitMethod() {
		String strSplit = "hello|trupti|devesh";
		String[] strArray = strSplit.split("\\|");
		for (String string : strArray) {
			System.out.println("String split:" + string);
		}
	}

	public String isSwapChar(String Str, int i, int j) {
		StringBuilder stringBuilder = new StringBuilder(Str);
		char c = stringBuilder.charAt(i);
		char d = stringBuilder.charAt(j);
		stringBuilder.setCharAt(i, d);
		stringBuilder.setCharAt(j, c);
		return stringBuilder.toString();
	}

	public boolean isCharSequenceMethod(String sentence, CharSequence sequence) {
		boolean seq = sentence.contains(sequence);
		if (!seq) {
			return false;
		}
		return seq;
	}

	public boolean isEqualIgnoreCase(String name, String name1) {
		if (name.equalsIgnoreCase(name1)) {
			return true;
		} else {
			return false;
		}
	}

	public String isAppendMethod(String name, String name1) {
		StringBuilder builder = new StringBuilder(name);
		String afterConcat = builder.append(name1).toString();
		return afterConcat;
	}

	public String isUsingStringBuilder(char a, char b, char c, char d) {
		String str;
		StringBuilder builder = new StringBuilder();
		builder.append(a);
		builder.append(b);
		builder.append(c);
		builder.append(d);
		str = builder.toString();
		return str;
	}

	public String isAddChar(String str, char ch, int position) {
		String afterAdd;
		StringBuilder builder = new StringBuilder(str);
		builder.insert(position, ch);
		return afterAdd = builder.toString();
	}
	public boolean isEmptyMethod(String str) {
		if(str.isEmpty()) {
			return true;
		}else {
			return false;
		}
	}
	public String isReplace(String str, char o,char n) {
		String replaceString = str.replace(o, n);
		return replaceString;
	}
	public static void main(String[] args) {
		/*
		 * String fileName = "......";
		 * 
		 * System.out.println(fileName.substring(fileName.lastIndexOf(".")));
		 */
		AllStringMethod allStringMethod = new AllStringMethod();
	System.out.println(allStringMethod.isReplace("Mamonu", 'o', 'u'));
		//System.out.println(allStringMethod.isEmptyMethod("hj"));
		// System.out.println("After concatation is:" +
		// allStringMethod.isUsingStringBuilder('2','/','3','/'));
		//String str = "Truptimaye";
		//int position = str.length() - 1;
		//System.out.println("After Add Char is:" + allStringMethod.isAddChar(str, 'e', position));
		// System.out.println(allStringMethod.getIndexOf("MUNU@Dash", 'D'));
		// imutabeTest.isSplitMethod();
		// System.out.println(allStringMethod.isCharAt("Java8"));
		// System.out.println("New SubString Is:"+allStringMethod.isSubStringMethod("My
		// Baba", 3));
		// System.out.println("SubString found?:" +
		// allStringMethod.isCharSequenceMethod("Hello World", "He"));
		// String st = "BAba";
		// System.out.println("Swap String is:"+allStringMethod.isSwapChar(st,0,1));
		// System.out.println("Swap String is:"+allStringMethod.isSwapChar(st,0,
		// st.length()-1));
	}

}
