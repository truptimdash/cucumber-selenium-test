package com.trupti;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeleteRowFromTable {
	String delete = "delete from student where roll_no=? ";
	PreparedStatement statement;
	Connection conn;
	public void  deleteRecord(int rollNo)  {
		
		//Student student = new Student();
		try {
			JavaToPostgresConnection connection = new JavaToPostgresConnection();
			conn = connection.conect();
			statement = conn.prepareStatement(delete);
			statement.setInt(1, rollNo);
			int count= statement.executeUpdate();
			System.out.println(count);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		DeleteRowFromTable selectRecord = new DeleteRowFromTable();
				 selectRecord.deleteRecord(11);
	}
}
