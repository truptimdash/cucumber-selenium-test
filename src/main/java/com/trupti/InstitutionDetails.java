package com.trupti;

public class InstitutionDetails {
	private String intitutionId;
	private String acronym;
	private String officialId	;
	private String institutionName	;
	private String englishName	;
	private String url;
	private String email;
	private String fax;
	private String phone;
	
	public String getIntitutionId() {
		return intitutionId;
	}
	public void setIntitutionId(String intitutionId) {
		this.intitutionId = intitutionId;
	}
	public String getAcronym() {
		return acronym;
	}
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
	public String getOfficialId() {
		return officialId;
	}
	public void setOfficialId(String officialId) {
		this.officialId = officialId;
	}
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	

}
