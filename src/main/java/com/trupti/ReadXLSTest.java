package com.trupti;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadXLSTest {
	public List<Student> readXLSTest() throws IOException {
		List<Student> list = new ArrayList<Student>();
		String fileLocation = "E:\\SeleniumProject\\cucumber-selenuim-test\\src\\Student.xlsx";
		try (FileInputStream file = new FileInputStream(new File(fileLocation));
				Workbook workbook = new XSSFWorkbook(file)) {

			Sheet sheet = workbook.getSheetAt(0);
			int numberOfRows = sheet.getPhysicalNumberOfRows();
			for (int i = 1; i < numberOfRows; i++) {
				Student student = new Student();
				Row row = sheet.getRow(i);
				String rollNo = String.valueOf(row.getCell(0).getNumericCellValue());
				String stuName = row.getCell(1).getStringCellValue();
				String subject = row.getCell(2).getStringCellValue();

				student.setStuName(stuName);
				student.setSubject(subject);
				student.setRollNo(rollNo);
				list.add(student);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static void main(String[] args) throws IOException {
		ReadXLSTest readXLSTest = new ReadXLSTest();
		List<Student> list = readXLSTest.readXLSTest();
		for (Student student : list) {
			System.out.println(student.getRollNo() + " " + student.getStuName() + "  " + "  " + student.getSubject());
		}
	}
}
