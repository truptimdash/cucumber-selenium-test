package com.trupti;

public class EUOfficialLanguages {
	private String euId;
	private String euOfficialLan;

	public String getEuId() {
		return euId;
	}

	public void setEuId(String euId) {
		this.euId = euId;
	}

	public String getEuOfficialLan() {
		return euOfficialLan;
	}

	public void setEuOfficialLan(String euOfficialLan) {
		this.euOfficialLan = euOfficialLan;
	}

}
