package com.trupti;

public class IfElseStamentTest {
	public boolean isPositive(int num) {
		if (num > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isNegative(int num) {
		if (num < 0) {
			return true;
		} else {
			return false;
		}
	}

//Take values of length and breadth of a rectangle from user and check if it is square or not
	public boolean isSquare(int length, int breadth) {
		if (length == breadth) {
			return true;
		} else {
			return false;
		}
	}

	// Take two int values from user and print greatest among them.
	public boolean isGreater(int a, int b) {
		if (a > b) {
			return true;
		} else {
			return false;
		}
	}

//Take input of age of 3 people by user and determine oldest and youngest among them
	public boolean isyoungest(int a, int b, int c) {
		if ((a >= b) && (b > c)) {
			return true;
		} else {
			return false;
		}
	}

	// Take input of ch and check upper case and lower case
	public boolean isUpperCase(String word) {
		if (word != null) {
			return word.toUpperCase().equals(word);

		} else {
			return false;
		}

	}
	//Check Even or Odd
	public boolean isEven(int number)
	{
		if(number%2==0) {
			return true;
		}else {
			return false;
		}
	}
	

	public static void main(String[] args) {
		IfElseStamentTest elseStamentTest = new IfElseStamentTest();
		boolean num = elseStamentTest.isEven(3);
		System.out.println(num);
	}
}
