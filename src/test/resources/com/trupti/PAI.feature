Feature: 
      Here I will test the PAI

  Scenario Outline: Test Public Access Interface
    Given I nevigates to the specific URL
    When I Select to the Country "<Country Row>"
    And I Select the Page Items "<Page Items>"
    Then I Validate "Acronym" "<Acronym>" for Row "<Row>"
    And I Validate "Official Id" "<Official Id>" for Row "<Row>"
    And I Validate "Institution Name" "<Institution Name>" for Row "<Row>"
    And I Validate "English Name" "<English Name>" for Row "<Row>"

    Examples: 
      | Country Row | Page Items | Row | Acronym   | Official Id | Institution Name                              | English Name                    |
      |          32 |         10 |   1 | 0100 - CM |        0100 | Der Landesbund der Christlichen Krankenkassen | Christian health insurance fund |

  Scenario Outline: Test1 Public Access Interface
    Given I nevigates to the specific URL
    When I Select to the Country "<Country Row>"
    And I Click the Search Button
    And I Click On View Where Row Is "<Row>"
    Then I Validate Main Info "Institution Name" "<Institution Name>"
    And I Validate Main Info "English Name" "<English Name>"
    And I Validate Main Info "Acronym" "<Acronym>"
    And I Validate Main Info "Official Id" "<Official Id>"
    And I Validate Main Info "Status" "<Status>"
    And I Validate Main Info "Issues EHIC" "<Issues EHIC>"
    And I Validate Main Info "Validity Period" "<Validity Period>"

    Examples: 
      | Country Row | Row | Institution Name | English Name | Acronym | Official Id | Status | Issues EHIC | Validity Period |

  #  |           2 |   1 | Der Landesbund der Christlichen Krankenkassen | Christian health insurance fund | 0100 - CM |        0100 | Active | false        | - Indeterminate |
  Scenario Outline: Test2 Public Access Interface
    Given I nevigates to the specific URL
    When I Select to the Country "<Country Row>"
    And I Click the Search Button
    And I Click On View Where Row Is "<Row>"
    Then I Validate Contact Information "URLs" "<URLs>"
    And I Validate Contact Information "Email Addresses" "<Email Addresses>"
    #And I Validate Contact Information "Fax Numbers" "<Fax Numbers>"
    And I Validate Contact Information "Phone Numbers" "<Phone Numbers>"

    Examples: 
      | Country Row | Row | URLs | Email Addresses | Phone Numbers |

  #|2					|1	|www.mc.be|landsbond@cm.be|+32 2 246 41 11|
  Scenario Outline: Test3 Public Access Interface
    Given I nevigates to the specific URL
    When I Select to the Country "<Country Row>"
    And I Click the Search Button
    And I Click On View Where Row Is "<Row>"
    And I Select Choose The Official Language "<Language>"
    And I Click History Button
    Then I Validate the Institution History "Updated" "<Updated>" for Row "<Row>"
    And I Validate the Institution History "Acronym	" "<Acronym>" for Row "<Row>"
    And I Validate the Institution History "Official Id" "<Official Id>" for Row "<Row>"
    And I Validate the Institution History "Institution Name" "<Institution Name>" for Row "<Row>"
    And I Validate the Institution History "English Name" "<English Name>" for Row "<Row>"

    Examples: 
      | Country Row | Row | Language | Updated | Acronym | Official Id | Institution Name | English Name |

  # |2						|1	|Deutsch|10/11/2022	|0100 - CM|0100				|Der Landesbund der Christlichen Krankenkassen|Christian health insurance fund|
  Scenario Outline: Test4 Public Access Interface
    Given I nevigates to the specific URL
    When I Click the Employment, Social Affairs & Inclusion Link
    And I Click Event
    And I Select Year "<Year>"
    And I Select Country "<Country>"
    And I Select Section "<Section>"
    And I Select EventType "<Event type>"
    Then I Validate Event "<Event>"

    Examples: 
      | Year | Country | Section | Event type | Event |
  # | 2022 | France  | Select a main category | Meeting    | French Presidency and European Commission join forces to give new impetus to the fight against homelessness |
  
  Scenario: Test5 Public Access Interface
    Given I nevigates to the specific URL
    When I Click the Employment, Social Affairs & Inclusion Link
    And I Click the Policies and activities
    Then I Could See all the Links

  Scenario Outline: Test6 Public Access Interface
    Given I nevigates to the specific URL
    When I Click On Contact Info DG
    And I want to fill contact Info DG
    And "First Name" is "<First Name>"
    And "Last Name" is "<Last Name>"
    And "email" is "<email>"
    And "Nationality" is "<Nationality>"
    And "Residense" is "<Residense>"
    And "Langugae" is "<Langugae>"
    And "Enquiry" is "<Enquiry>"
    And I accept declaration
    And I click on Submit for contact Info DG
    Then I see "<status>" page

    Examples: 
      | First Name | Last Name | email          | Nationality | Residense      | Langugae     | Enquiry | status  |
      | Debashish  |           | test@gmail.com | British     | United Kingdom | English (en) | asd     | fail    |
      | debashish  | dash      | test@gmail.com | British     | United Kingdom | English (en) | asd     | Success |

  Scenario: Test7 Public Access Interface
    Given I nevigates to the specific URL
    When I Click On Information about the DG
    And I Click On Meetings held by the Director-General
    Then I validate Meetings held by the Director-General
 
 
   Scenario Outline: Test8 Public Access Interface
    Given I nevigates to the specific URL
    And I Click On EN BOx
    And I Validate EUOfficialLanguages
    When I Click EU official languages "<Language>"
    Then I redurect to language "<Language>" url
   
    
    Examples:
    			|Language	|
    		#	|eesti		|
    			#|English	|