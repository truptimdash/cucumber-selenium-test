package com.trupti.init;

import java.time.Duration;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;

public class CommonWebDriver {
	private static WebDriver driver;
	private static String CHOME_DRIVER_PATH = "D:/app/chromedriver/chromedriver.exe";

	public static WebDriver getWebDriver() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver", CHOME_DRIVER_PATH);

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--incognito");
			options.addArguments("--remote-allow-origins=*");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			options.setCapability(ChromeOptions.CAPABILITY, options);
			options.merge(capabilities);

			WebDriver webDriver = new ChromeDriver(options);
			webDriver.manage().window().maximize();
			webDriver.manage().deleteAllCookies();
			webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
			driver = webDriver;
		}
		return driver;
	}

	public static WebDriver getWebDriverHeadless() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver", "E:/App/chromedriver.exe");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--no-sandbox");
			chromeOptions.addArguments("--headless");
			chromeOptions.addArguments("disable-gpu");
			driver = new ChromeDriver(chromeOptions);
			driver.get("https://ec.europa.eu/social/social-security-directory/pai/select-country/language/en");
			return driver;
		}
		return driver;
	}
@After(order=1)
public void takeScreenshot(Scenario scenario) {
	System.out.println("Taking the Screenshot");
	if(scenario.isFailed()) {
		TakesScreenshot takesScreenshot = (TakesScreenshot)driver;
		byte[] src = takesScreenshot.getScreenshotAs(OutputType.BYTES);
		scenario.attach(src, "img/png", "screenshot");
		
	}
}
	public static void close() {
		if (null != driver) {
			// driver.close();
		}
	}
}
