package com.trupti.init;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverBuilder {

	private static final String BROWSER_EXE_PATH = "D:/app/chromedriver/chromedriver.exe";

	public static WebDriver buildWebDriver() {
		WebDriver webDriver = null;
		System.setProperty("BROWSER", "Chrome");
		String browser = System.getProperty("BROWSER") == null ? "headless" : System.getProperty("BROWSER");
		switch (browser) {
		case "headless":
			System.setProperty("webdriver.chrome.driver", BROWSER_EXE_PATH);

			webDriver = new ChromeDriver(getChromeOptions(true));
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", BROWSER_EXE_PATH);
			webDriver = new ChromeDriver(getChromeOptions(false));
			break;
		case "Firefox":
			break;
		case "IE":
			break;
		case "Edge":
			break;
		case "Safari":
			break;
		default:
			System.setProperty("webdriver.chrome.driver", BROWSER_EXE_PATH);
			webDriver = new ChromeDriver();
			break;
		}

		webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		webDriver.manage().deleteAllCookies();
		webDriver.manage().window().maximize();
		return webDriver;
	}

	private static ChromeOptions getChromeOptions(boolean headless) {
		// Accepts Insured Certificates
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setBrowserName("Chrome");
		// derCapabilities.acceptInsecureCerts();
		capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		// capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");
		if (headless) {
			options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200",
					"--ignore-certificate-errors");
		}

		options.merge(capabilities);
		return options;
	}

}
