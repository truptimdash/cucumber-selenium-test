package com.trupti.runner;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstSeleniumTest {

  public static void main(String[] args) throws Exception {
    System.setProperty("webdriver.chrome.driver", "E:/App/chromedriver.exe");
    WebDriver webDriver = new ChromeDriver();
    // webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    webDriver.get("https://phptravels.net/api/admin");
    // Below to be moved to WebDriverBuilder in Real world test scenario
    // webDriver.findElement(By.name("email")).sendKeys("admin@phptravels.com");
    webDriver.findElement(By.cssSelector("input[name='email']")).sendKeys("admin@phptravels.com");
    webDriver.findElement(By.name("password")).sendKeys("demoadmin");
    webDriver.findElement(By.cssSelector("button[type='submit']")).click();
    // <div class="display-5">1822</div>

    Thread.sleep(5000);
    System.out
        .println(webDriver.findElement(By.cssSelector("h1[class='display-4 mb-0']")).getText());

    List<WebElement> elements = webDriver.findElements(By.cssSelector("div[class='display-5']"));
    for (WebElement webElement : elements) {
      System.out.println(webElement.getText());
    }
   System.out.println(webDriver.findElements(By.cssSelector("div[class='card-text']")).get(3).getText());

   //webDriver.close();
   // <div class="card-text">Confrimed Bookings</div>
    // CSS selector example
    // https://devqa.io/selenium-css-selectors/

  }

}
