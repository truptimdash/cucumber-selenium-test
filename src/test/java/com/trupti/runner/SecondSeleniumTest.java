package com.trupti.runner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SecondSeleniumTest {

  public static void main(String[] args) throws Exception {
    System.setProperty("webdriver.chrome.driver", "E:/App/chromedriver.exe");
    WebDriver webDriver = new ChromeDriver();
    // webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    webDriver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
    webDriver.findElement(By.cssSelector("input[value='radio1']")).click();
    
    System.out.println(webDriver.findElement(By.cssSelector("input[value='radio1']")).isSelected());
    webDriver.findElement(By.cssSelector("input[value='radio2']")).click();
    System.out.println(webDriver.findElement(By.cssSelector("input[value='radio2']")).isSelected());
    
    webDriver.findElement(By.cssSelector("input[id='checkBoxOption1']")).click();
    
    System.out.println(webDriver.findElement(By.cssSelector("input[id='checkBoxOption1']")).isSelected());
    
    webDriver.findElement(By.cssSelector("input[id='checkBoxOption2']")).click();
    System.out.println(webDriver.findElement(By.cssSelector("input[id='checkBoxOption2']")).isSelected());
    
    webDriver.findElement(By.cssSelector("input[id='checkBoxOption3']")).click();
    System.out.println(webDriver.findElement(By.cssSelector("input[id='checkBoxOption3']")).isSelected());
    
    
    
    WebElement dropdown= webDriver.findElement(By.cssSelector("select[id='dropdown-class-example']"));
    dropdown.sendKeys("Option2");
    
    Select select = new Select(dropdown);
    System.out.println(select.getFirstSelectedOption().getText());
    
    //Web table https://www.guru99.com/handling-dynamic-selenium-webdriver.html
  }

}
